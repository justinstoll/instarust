# instarust

This is a web application based on [axum](https://github.com/tokio-rs/axum).

## Build and Run

```
docker build -t instarust:latest .
docker run --rm -p 3000:3000 instarust:latest
```

Access your web browser at [http://localhost:3000/](http://localhost:3000/) and you should see a "Hello, World" message.
