# DevOps Assignment

Instarust is a very simple web application written in Rust. The application itself works well, but the whole project setup is far from ideal. Soon we want to release instarust to production and also add cool new features to it. We want to run in on Kubernetes using Docker. **You need to help us with that**.

Your job is to extend and improve the currently existing GitLab CI/CD pipeline described in `.gitlab-ci.yml`.

## Current Setup

CI/CD jobs running in this project are using shared GitLab runners. The runners can connect to the internet, but are not accessible from the outside. The runners can pull and run any publicly available Docker image. Usage of Docker in Docker is also possible.

Currently, only one linting job is run on every commit that is pushed to any branch of the respository.

Check the [official GitLab CI/CD documentation](https://docs.gitlab.com/ee/ci/pipelines/) for details.

## Assignment

**You should not spend more than 2-3 hours on this assignment!**

The outcome of this assignemnt should be a merge request in GitLab that contains your changes to the project, as well as a written description of the tasks that don't require any changes to the code. You can answer the questions in any format or tool you deem suitable, just make sure to make your answer accessible to us.

You can work in this GitLab project, or fork it into your own GitLab project. In any case we need access to the code, once you are finished.

If you have any questions about the task or encounter any blockers, feel free to message us anytime!

### Building a Docker image

We need to automatically build and publish an Instarust Docker image in the CI/CD pipeline in GitLab. The pipeline should work like so:

1. Every commit pushed in the context of a merge request should trigger a build
2. If the build is successful, the resulting Docker image is pushed to the GitLab container registry of the project
3. Developers should be able to look up the image name in the logs of the CI/CD job
4. There is only one image per merge request in the container registry
5. When the merge request gets merged, the image belonging to that merge request is removed from the container registry

#### Optimising the Pipeline

1. We pay for bandwidth and storage for everything that happens in the CI/CD pipelines. Please try to make the resulting Docker image more lightweight in order to save transer and storage cost.
2. A lot of developers are working on the project and they are constantly waiting for their CI/CD pipelines. Please provide a few ideas, how we could speed up the jobs given that we run at least 100 pipelines a day in different merge requests. You don't have to implement the ideas, a written descriptions is enough.

### Optimising the Pipeline - Answer

2. In order to increase the performance of the pipelines, there are multiple ways:
    - Parallelize the pipeline stages if possible
    - Increase resources (preferably using spot machines, which would also reduce the cost drastically)
    - Optimize the images being used (reduce size by using slim images and remove uneccesary dependencies)
    - We could use an Image Proxy to prevent rate limiting from Docker Hub (for any third party docker image)

#### Monitoring and Alerting Concept

1. Describe, how you would go about implementing a monitoring and alerting concept for Instarust, once it is running in Kubernetes on production. The goal of the monitoring and alerting concept is to identify problems with the service proactively and enable you and the developers to debug and solve them. You can make any assumptions about the rest of our tech stack.

### Monitoring and Alerting Concept - Answer

There are two components, that I would monitor:
1. Resources (using Prometheus, we can fetch the system usage, aswell as pod restarts (if the liveness and readiness checks for the pod are properly configured and usable) and display them in Grafana. This way we can identify issues that occur due to resource exhaustion or misconfiguration)
2. Service itself (with fluentd, we could fetch the logs of the service itself. After fetching them and converting them in a properly readible format, they can be pushed to elasticsearch, which we can then use as a datasource for Grafana. Similar to the resource usage, we could also create Grafana alerts for errors that appear on the application)
Depending on the Communication tool that you use (Slack, Teams or others) we can send alerts there with webhooks or use a SMTP user to send mails from Grafana directly. This should be preferebly done to a shared mailbox or a shared channel in the communication tool of your choice, to make sure that there is no single dependency on personal)


#### Runtime Configuration in Kubernetes

1. Describe, how you would go about managing different runtime configurations for Instarust for different environments when running Instarust in a Kubernetes cluster. For example, in a staging environment we might want to use a different log level than on a production environment. Assume that all runtime configuration is done via environment variables.

### Runtime Configuration in Kubernetes - Answer

Different Runtime Configurations can be easily implemented using helm charts if not already existing. We can fetch the different environment variables either using Gitlab CI/CD Variables or using Environment Variable Files which we will then use to replace the variables inside the helm values file. Preferebly, I would do this using Ansible.